//
//  TransactionModel.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright © 2018 Genar Codina Reverter. All rights reserved.
//

import Foundation

struct TransactionModel: Codable {
    let account, balance: String?
    let transactions: [Transaction]?
}

struct Transaction: Codable {
    let id, amount, description, otherAccount: String?
    let date: Date?
}
