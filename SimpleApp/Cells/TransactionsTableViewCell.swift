//
//  TransactionsTableViewCell.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright © 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

class TransactionsTableViewCell: UITableViewCell {

    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var afterLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var beforeLabel: UILabel!
    @IBOutlet var idLabel: UILabel!

    static var cellType: String {
        return String(describing: self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none
    }

    func displayInfo(id: String, before: String, amount: String, after: String, date: String) {
        idLabel.text = id
        beforeLabel.text = before
        amountLabel.text = amount
        afterLabel.text = after
        dateLabel.text = date
    }

}
