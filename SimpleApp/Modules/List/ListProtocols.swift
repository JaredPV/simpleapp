//
//  ListProtocols.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

protocol ListPresenterToViewProtocol: class {
    var transactionsLabel: UILabel! {get set}
    var accountLabel: UILabel! {get set}
    var messageLabel: UILabel! {get set}
    var transactionTableView: UITableView! {get set}
    var formatedData: [ListModel]? {get set}
    func refresh()
}

protocol ListInteractorToPresenterProtocol: class {
    func transactionFetchedSuccess(_ model: TransactionModel)
    func transactionFetchedError(_ error: Error)
    func preparedFormatedModel(_ model: [ListModel])
}

protocol ListPresenterToInteractorProtocol: class {
    var presenter: ListInteractorToPresenterProtocol? {get set}
    func fetchTransactionModel()
    func prepareFormatedModel(_ model: TransactionModel)
}

protocol ListViewToPresenterProtocol: class {
    var view: ListPresenterToViewProtocol? {get set}
    var interactor: ListPresenterToInteractorProtocol? {get set}
    var router: ListPresenterToRouterProtocol? {get set}
    func updateView()
    func rowSelected(_ row: Int)
}

protocol ListPresenterToRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateToDetail(_ origin: UIViewController, _ model: Transaction)
}
