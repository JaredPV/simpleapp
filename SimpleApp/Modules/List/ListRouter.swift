//
//  ListRouter.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

class ListRouter: ListPresenterToRouterProtocol {

    class func createModule() -> UIViewController {
        guard let view = mainstoryboard.instantiateViewController(withIdentifier: "ListViewController") as? ListViewController else {return UIViewController()}
        let presenter: ListViewToPresenterProtocol & ListInteractorToPresenterProtocol = ListPresenter()
        let interactor: ListPresenterToInteractorProtocol = ListInteractor()
        let router: ListPresenterToRouterProtocol = ListRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter

        return view

    }

    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }

    func navigateToDetail(_ origin: UIViewController, _ model: Transaction) {
        let detail = DetailRouter.createModule(model: model)
        origin.navigationController?.pushViewController(detail, animated: true)
    }
}
