//
//  ListPresenter.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

class ListPresenter: ListViewToPresenterProtocol {

    var view: ListPresenterToViewProtocol?
    var interactor: ListPresenterToInteractorProtocol?
    var router: ListPresenterToRouterProtocol?

    private var data: TransactionModel?

    func updateView() {
        showLoader()
        view?.transactionTableView.register(UINib(nibName: TransactionsTableViewCell.cellType, bundle: nil), forCellReuseIdentifier: TransactionsTableViewCell.cellType)
        view?.transactionTableView.rowHeight = 180
        view?.transactionTableView.bounces = false
        view?.transactionTableView.delegate = view as? UITableViewDelegate
        view?.transactionTableView.dataSource = view as? UITableViewDataSource
        view?.transactionTableView.tableFooterView = UIView()
        interactor?.fetchTransactionModel()
    }

    func rowSelected(_ row: Int) {
        guard let view = view as? UIViewController, let transactions = data?.transactions else { return }
        router?.navigateToDetail(view, transactions[row])
    }
}

// MARK: - Loader
extension ListPresenter {
    private func showLoader() {
        guard let view = view as? UIViewController else {return}
        view.showLoader()
    }

    private func hideLoader() {
        guard let view = view as? UIViewController else {return}
        view.hideLoader()
    }
}

// MARK: - InteractorToPresenter
extension ListPresenter: ListInteractorToPresenterProtocol {
    func preparedFormatedModel(_ model: [ListModel]) {
        view?.formatedData = model
        view?.refresh()
    }

    func transactionFetchedSuccess(_ model: TransactionModel) {
        hideLoader()
        self.data = model
        view?.messageLabel.text = model.account
        interactor?.prepareFormatedModel(model)
    }

    func transactionFetchedError(_ error: Error) {
        hideLoader()
    }
}
