//
//  ListInteractor.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

class ListInteractor: ListPresenterToInteractorProtocol {

    var presenter: ListInteractorToPresenterProtocol?

    func fetchTransactionModel() {
        if let file = Bundle.main.path(forResource: "transactions", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: file)) {
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let object = try? decoder.decode(TransactionModel.self, from: data)
                guard let transaction = object, let transactionsList = transaction.transactions  else {throw CustomError.WrongParser("wrongParser")}
                let orderedList = orderTransactionsByAscendingDate(transactionsList)
                let orderedTransaction = TransactionModel(account: transaction.account, balance: transaction.balance, transactions: orderedList)
                presenter?.transactionFetchedSuccess(orderedTransaction)
            } catch(let error) {
                presenter?.transactionFetchedError(error)
            }
        }
    }

    private func orderTransactionsByAscendingDate(_ input: [Transaction]) -> [Transaction] {
        let result = input.sorted(by: {
            if let date1 = $0.date, let date2 = $1.date {
                return date1.compare(date2) == .orderedAscending
            }
            return false
        })
        return result
    }

    func prepareFormatedModel(_ model: TransactionModel) {
        guard let list = model.transactions else {return}
        guard let balanceString = model.balance else {return}
        guard var balance = Decimal(string: balanceString) else {return}
        var result = [ListModel]()
        for item in list {
            let id = item.id ?? ""
            let description = item.description ?? ""
            let formatedID = "\(id) - \(description)"
            let amountString = item.amount ?? ""
            guard let amount = Decimal(string: amountString) else {return}
            let before = Utils.formatDecimal(decimal: balance)
            let after = balance + amount
            balance = after
            let afterString = Utils.formatDecimal(decimal: after)
            var date = ""
            if let dateString = item.date {
                date = dateString.string(with: "MMM dd, yyyy, HH:mm:ss", useUTC: true)
            }
            let object = ListModel(id: formatedID, before: before, amount: amountString, after: afterString, date: date)
            result.append(object)
        }
        presenter?.preparedFormatedModel(result)
    }

}
