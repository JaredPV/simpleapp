//
//  ListEntities.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

struct ListModel {
    var id: String
    var before: String
    var amount: String
    var after: String
    var date: String
}

enum CustomError: Error {
    case WrongParser(String)
}
