//
//  ViewController.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 03/08/2018.
//  Copyright © 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {

    var presenter: ListViewToPresenterProtocol?

    // MARK: - Outlets
    @IBOutlet weak var transactionsLabel: UILabel!
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var transactionTableView: UITableView!

    // MARK: - Attributes
    var formatedData: [ListModel]?

    // MARK: - UIViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter?.updateView()
    }

}

// MARK: - Table View DataSource
extension ListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TransactionsTableViewCell.cellType, for: indexPath) as? TransactionsTableViewCell else {return UITableViewCell()}
        guard let data = formatedData else {return UITableViewCell()}
        let element = data[indexPath.row]
        cell.displayInfo(id: element.id, before: element.before, amount: element.amount, after: element.after, date: element.date)
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let numberOfElements = formatedData?.count {
            return numberOfElements
        }
        return 0
    }
}

// MARK: - Table View Delegate
extension ListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.rowSelected(indexPath.row)
    }
}

extension ListViewController: ListPresenterToViewProtocol {
    func refresh() {
        transactionTableView.reloadData()
    }
}
