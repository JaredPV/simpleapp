//
//  DetailProtocols.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

protocol DetailPresenterToViewProtocol: class {
    var idAccountLabel: UILabel! { get set }
    var descriptionTitleLabel: UILabel! { get set }
    var descriptionLabel: UILabel! { get set }
    var amountTitleLabel: UILabel! { get set }
    var amountLabel: UILabel! { get set }
    var otherAccountTitleLabel: UILabel! { get set }
    var otherAccountLabel: UILabel! { get set }
    var dateTitleLabel: UILabel! { get set }
    var dateLabel: UILabel! { get set }
    var transaction: Transaction? { get set }
}

protocol DetailInteractorToPresenterProtocol: class {}

protocol DetailPresenterToInteractorProtocol: class {
    var presenter: DetailInteractorToPresenterProtocol? {get set}
}

protocol DetailViewToPresenterProtocol: class {
    var view: DetailPresenterToViewProtocol? {get set}
    var interactor: DetailPresenterToInteractorProtocol? {get set}
    var router: DetailPresenterToRouterProtocol? {get set}
    func updateView()
    func fillTitles()
}

protocol DetailPresenterToRouterProtocol: class {
    static func createModule(model: Transaction?) -> UIViewController
}
