//
//  DetailPresenter.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

class DetailPresenter: DetailViewToPresenterProtocol {

    var view: DetailPresenterToViewProtocol?
    var interactor: DetailPresenterToInteractorProtocol?
    var router: DetailPresenterToRouterProtocol?

    func updateView() {
        guard let view = view, let transaction = view.transaction else { return }
        view.idAccountLabel.text = transaction.id
        view.descriptionLabel.text = transaction.description
        view.amountLabel.text = transaction.amount
        Utils.setTextColor(number: Decimal(string: transaction.amount ?? "0") ?? 0, label: view.amountLabel)
        view.otherAccountLabel.text = transaction.otherAccount
        guard let formattedTransaction = transaction.date?.string(with: "MMM dd, yyyy, HH:mm:ss", useUTC: true) else {
            view.dateLabel.text = ""
            return
        }
        view.dateLabel.text = formattedTransaction + " UTC"
    }

    func fillTitles() {
        guard let view = view else { return }
        let amountTitle = NSLocalizedString(LocalizedEnum.DetailTitle.detailAmountTitle, comment: "")
        view.amountTitleLabel.text = amountTitle

        let descriptionTitle = NSLocalizedString(LocalizedEnum.DetailTitle.detailDesciptionTitle, comment: "")
        view.descriptionTitleLabel.text = descriptionTitle

        let otherAccountTitle = NSLocalizedString(LocalizedEnum.DetailTitle.detailOtherAccountTitle, comment: "")
        view.otherAccountTitleLabel.text = otherAccountTitle

        let dateTitle = NSLocalizedString(LocalizedEnum.DetailTitle.detailDateTitle, comment: "")
        view.dateTitleLabel.text = dateTitle
    }
}

extension DetailPresenter: DetailInteractorToPresenterProtocol {}
