//
//  DetailRouter.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

class DetailRouter: DetailPresenterToRouterProtocol {

    class func createModule(model: Transaction? = nil) -> UIViewController {
        guard let view = mainstoryboard.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else { return UIViewController() }
        let presenter: DetailViewToPresenterProtocol & DetailInteractorToPresenterProtocol = DetailPresenter()
        let interactor: DetailPresenterToInteractorProtocol = DetailInteractor()
        let router: DetailPresenterToRouterProtocol = DetailRouter()

        view.presenter = presenter
        view.transaction = model
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter

        return view
    }

    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
