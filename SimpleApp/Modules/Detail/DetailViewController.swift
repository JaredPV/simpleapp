//
//  DetailViewController.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var idAccountLabel: UILabel!
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var amountTitleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var otherAccountTitleLabel: UILabel!
    @IBOutlet weak var otherAccountLabel: UILabel!
    @IBOutlet weak var dateTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    var transaction: Transaction?

    var presenter: DetailViewToPresenterProtocol?

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.fillTitles()
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        presenter?.updateView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension DetailViewController: DetailPresenterToViewProtocol {}
