//
//  DetailInteractor.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

import UIKit

class DetailInteractor: DetailPresenterToInteractorProtocol {

    var presenter: DetailInteractorToPresenterProtocol?
}
