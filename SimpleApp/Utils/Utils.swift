//
//  Utils.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/2018.
//  Copyright © 2018 Genar Codina Reverter. All rights reserved.
//

import Foundation
import UIKit

enum Utils {

    static func setTextColor(number: Decimal, label: UILabel) {

        if number < 0 {

            label.textColor = UIColor.red
        } else {
            label.textColor = UIColor.blue
        }
    }

    static func formatDecimal(decimal: Decimal) -> String {

        let decimalNumber  = NSDecimalNumber(decimal: decimal)
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        formatter.locale = Locale.current
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        let numberString = formatter.string(from: decimalNumber) ?? ""

        return numberString
    }
}
