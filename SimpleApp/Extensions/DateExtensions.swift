//
//  DateExtensions.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/2018.
//  Copyright © 2018 Genar Codina Reverter. All rights reserved.
//

import Foundation

extension Date {

    func string(with format: String, useUTC isUTC: Bool) -> String {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if isUTC {
            dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        }
        return dateFormatter.string(from: self)
    }
}
