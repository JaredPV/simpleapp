//
//  LocalizedEnum.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/2018.
//  Copyright © 2018 Genar Codina Reverter. All rights reserved.
//

import Foundation

enum LocalizedEnum {

    enum Account {

        static let account: String = "account"
        static let accountBalance: String = "account_balance"
    }

    enum Transaction {

        static let transactionAmount: String = "transaction_amount"
        static let transactionDescription: String = "transaction_description"
        static let transactionOtherAccount: String = "transaction_other_account"
        static let transactionDate: String = "transaction_date"
        static let transactions: String = "transactions"
    }

    enum CellTransaction {

        static let transactionBefore: String = "transaction_before"
        static let transactionAfter: String = "transaction_after"
        static let cellIdentifier: String = "TransactionCell"
    }

    enum Segue {

        static let segueFromListToDetails: String = "segueFromListToDetails"
    }

    enum Error {

        static let unableGetAccount: String = "unable_retrieve_account"
    }

    enum DetailTitle {

        static let detailDesciptionTitle = "detail_description_title"
        static let detailAmountTitle = "detail_amount_title"
        static let detailOtherAccountTitle = "detail_other_account_title"
        static let detailDateTitle = "detail_date_title"
    }
}
