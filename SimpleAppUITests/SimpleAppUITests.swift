//
//  SimpleAppUITests.swift
//  SimpleAppUITests
//
//  Created by Carles Grau Galvan on 21/08/2018.
//  Copyright © 2018 Genar Codina Reverter. All rights reserved.
//

import XCTest

class SimpleAppUITests: XCTestCase {

    let app = XCUIApplication()

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launch()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testFirstCellData() {
        let tablesQuery = app.tables
        let backButton = app.navigationBars["SimpleApp.DetailView"].buttons["Back"]
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["100.20"]/*[[".cells.staticTexts[\"100.20\"]",".staticTexts[\"100.20\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()

        let elementsQuery = app.scrollViews.otherElements
        XCTAssertTrue(elementsQuery.staticTexts["NL36MOYO0801604079"].exists)
        XCTAssertTrue(elementsQuery.staticTexts["May 12, 2018, 13:19:00 UTC"].exists)
        XCTAssertTrue(elementsQuery.staticTexts["-4.30"].exists)
        XCTAssertTrue(elementsQuery.staticTexts["starbucks amsterdam CS"].exists)
        backButton.tap()

    }

}
