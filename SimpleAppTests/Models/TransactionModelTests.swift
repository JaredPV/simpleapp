//
//  TransactionModelTests.swift
//  SimpleAppTests
//
//  Created by Carles Grau Galvan on 21/08/2018.
//  Copyright © 2018 Genar Codina Reverter. All rights reserved.
//

import XCTest
@testable import SimpleApp

class TransactionModelTests: XCTestCase {

    let decoder = JSONDecoder()
    var file: String!

    override func setUp() {
        super.setUp()
        decoder.dateDecodingStrategy = .iso8601
        file = Bundle.main.path(forResource: "transactions", ofType: "json")
    }

    override func tearDown() {
        super.tearDown()
    }

    func testModel() {
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: file)) else {
            XCTFail("unable to retrieve data from file")
            return
        }
        guard let object = try? decoder.decode(TransactionModel.self, from: data) else {
            XCTFail("unable to decode data with TransactionModel")
            return
        }
        XCTAssertNotNil(object.transactions)
        XCTAssertEqual(object.transactions?.count, 5)
        XCTAssertEqual(object.transactions?[0].id, "trx1")
        XCTAssertEqual(object.transactions?[0].amount, "-18.20")
        XCTAssertEqual(object.transactions?[0].description, "pizza")
    }

}
