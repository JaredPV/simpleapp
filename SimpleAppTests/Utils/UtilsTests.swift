//
//  UtilsTests.swift
//  SimpleAppTests
//
//  Created by Carles Grau Galvan on 04/08/18.
//  Copyright © 2018 Genar Codina Reverter. All rights reserved.
//

import XCTest
@testable import SimpleApp

class UtilsTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSetTextColorRedWhenMinus4dot30() {
        //given
        let label = UILabel()
        let number = Decimal.init(-4.30)

        //when
        Utils.setTextColor(number: number, label: label)

        //then
        XCTAssertEqual(label.textColor, UIColor.red)
    }

    func testSetTextColorRedWhenMinus750() {
        //given
        let label = UILabel()
        let number = Decimal.init(-750)

        //when
        Utils.setTextColor(number: number, label: label)

        //then
        XCTAssertEqual(label.textColor, UIColor.red)
    }

    func testSetTextColorBlueWhenPlus3dot01() {
        //given
        let label = UILabel()
        let number = Decimal.init(3.01)

        //when
        Utils.setTextColor(number: number, label: label)

        //then
        XCTAssertEqual(label.textColor, UIColor.blue)
    }

    func testSetTextColorBlueWhenPlus325dot32() {
        //given
        let label = UILabel()
        let number = Decimal.init(325.32)

        //when
        Utils.setTextColor(number: number, label: label)

        //then
        XCTAssertEqual(label.textColor, UIColor.blue)
    }

    func testFormatDecimal325dot32() {
        //given
        let decimal = Decimal.init(325.32)

        //when
        let result = Utils.formatDecimal(decimal: decimal)

        //then
        XCTAssertEqual(result, "325.32")
    }

    func testFormatDecimalMinus325dot32() {
        //given
        let decimal = Decimal.init(-325.32)

        //when
        let result = Utils.formatDecimal(decimal: decimal)

        //then
        XCTAssertEqual(result, "-325.32")
    }
}
