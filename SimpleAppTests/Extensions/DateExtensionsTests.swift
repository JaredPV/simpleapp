//
//  DateExtensionsTests.swift
//  SimpleAppTests
//
//  Created by Carles Grau Galvan on 04/08/18.
//  Copyright © 2018 Genar Codina Reverter. All rights reserved.
//

import XCTest
@testable import SimpleApp

class DateExtensionsTests: XCTestCase {

    var date = Date()

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testDateToStringFormatDate1() {
        //given
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        guard let someDateTime = formatter.date(from: "2018/08/21 22:31") else {
            return
        }
        date = someDateTime

        //when
        let result = date.string(with: "dd/MM/yyyy", useUTC: false)

        //then
        XCTAssertEqual(result, "21/08/2018")
    }

    func testDateToStringFormatDate2() {
        //given
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        guard let someDateTime = formatter.date(from: "2018/01/06 20:55") else {
            return
        }
        date = someDateTime

        //when
        let result = date.string(with: "HH:mm dd/MM/yyyy", useUTC: false)

        //then
        XCTAssertEqual(result, "20:55 06/01/2018")
    }

    func testDateToStringFormatIncorrectDate() {
        //given
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        guard let someDateTime = formatter.date(from: "2018/01/36 20:55") else {
            return
        }
        date = someDateTime

        //when
        let result = date.string(with: "HH:mm dd/MM/yyyy", useUTC: false)

        //then
        XCTAssertNotEqual(result, "20:55 36/01/2018")
    }
}
