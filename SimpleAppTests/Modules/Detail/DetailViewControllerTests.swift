//
//  ListViewControllerTests.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

@testable import SimpleApp
import XCTest

class DetailViewControllerTests: XCTestCase {
    // MARK: Subject under test

    var sut: DetailViewController!
    var spy: DetailViewLogicSpy!
    var window: UIWindow!

    // MARK: Test lifecycle

    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupDetailViewController()
    }

    override func tearDown() {
        window = nil
        super.tearDown()
    }

    // MARK: Test setup

    func setupDetailViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController

        spy = DetailViewLogicSpy()
        sut.presenter = spy
    }

    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }

    // MARK: Test doubles

    class DetailViewLogicSpy: DetailViewToPresenterProtocol {
        var view: DetailPresenterToViewProtocol?

        var interactor: DetailPresenterToInteractorProtocol?

        var router: DetailPresenterToRouterProtocol?

        var updateViewCalled = false
        var fillTitlesCalled = false

        func updateView() {
            updateViewCalled = true
        }

        func fillTitles() {
            fillTitlesCalled = true
        }

    }

    // MARK: Tests

    func testUpdateViewWhenViewIsLoaded() {
        // Given

        // When
        loadView()

        // Then
        XCTAssertTrue(spy.updateViewCalled, "updateView() should ask the presenter to update the view")
        XCTAssertTrue(spy.fillTitlesCalled, "fillTitles() should ask the presenter to fill the titles")
    }
}
