//
//  ListViewControllerTests.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

@testable import SimpleApp
import XCTest

class ListViewControllerTests: XCTestCase {
    // MARK: Subject under test

    var sut: ListViewController!
    var window: UIWindow!

    // MARK: Test lifecycle

    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupListViewController()
    }

    override func tearDown() {
        window = nil
        super.tearDown()
    }

    // MARK: Test setup

    func setupListViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
    }

    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }

    // MARK: Test doubles

    class ListViewLogicSpy: ListViewToPresenterProtocol {
        var view: ListPresenterToViewProtocol?

        var interactor: ListPresenterToInteractorProtocol?

        var router: ListPresenterToRouterProtocol?

        var updateViewCalled = false
        var rowSelectedCalled = false

        func updateView() {
            updateViewCalled = true
        }

        func rowSelected(_ row: Int) {
            rowSelectedCalled = true
        }
    }

    // MARK: Tests

    func testUpdateViewWhenViewIsLoaded() {
        // Given
        let spy = ListViewLogicSpy()
        sut.presenter = spy

        // When
        loadView()

        // Then
        XCTAssertTrue(spy.updateViewCalled, "updateView() should ask the presenter to update the view")
    }
}
