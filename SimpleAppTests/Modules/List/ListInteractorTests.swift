//
//  ListWorkerTests.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

@testable import SimpleApp
import XCTest

class ListInteractorTests: XCTestCase {
    // MARK: Subject under test

    var sut: ListInteractor!
    var spy: ListBusinessLogicSpy!

    // MARK: Test lifecycle

    override func setUp() {
        super.setUp()
        setupListInteractor()
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: Test setup

    func setupListInteractor() {
        sut = ListInteractor()
        spy = ListBusinessLogicSpy()
        sut.presenter = spy
    }

    class ListBusinessLogicSpy: ListInteractorToPresenterProtocol {
        var transactionFetchedSuccessCalled = false
        var transactionFetchedErrorCalled = false
        var preparedFormatedModelCalled = false

        var successModel: TransactionModel!
        var formattedModel: [ListModel]!

        func transactionFetchedSuccess(_ model: TransactionModel) {
            transactionFetchedSuccessCalled = true
            successModel = model
        }

        func transactionFetchedError(_ error: Error) {
            transactionFetchedErrorCalled = true
        }

        func preparedFormatedModel(_ model: [ListModel]) {
            preparedFormatedModelCalled = true
            formattedModel = model
        }
    }

    // MARK: Tests
    func testFetchTransactionModelSuccess() {
        // Given

        //when
        sut.fetchTransactionModel()

        // Then
        XCTAssertTrue(spy.transactionFetchedSuccessCalled, "transactionFetchedSuccess() should ask the presenter to do something")
        XCTAssertEqual(spy.successModel.account, "NL30MOYO0001234567")
        XCTAssertEqual(spy.successModel.balance, "100.20")
        XCTAssertEqual(spy.successModel.transactions?.count, 5)
    }

    func testFetchTransactionModelError() {
        // Given

        //when
        sut.fetchTransactionModel()

        // Then
        XCTAssertFalse(spy.transactionFetchedErrorCalled, "transactionFetchedError() should ask the presenter to do something")
    }

    func testPrepareFormatedModel() {
        // Given
        let model = TransactionModel(account: "bla bla", balance: "100", transactions: [
            Transaction(id: "1", amount: "100", description: "desc1", otherAccount: "otherAcc", date: Date.init()),
            Transaction(id: "2", amount: "110", description: "desc2", otherAccount: "otherAcc", date: Date.init()),
            Transaction(id: "3", amount: "120", description: "desc3", otherAccount: "otherAcc", date: Date.init()),
            Transaction(id: "4", amount: "130", description: "desc4", otherAccount: "otherAcc", date: Date.init()),
            Transaction(id: "5", amount: "140", description: "desc5", otherAccount: "otherAcc", date: Date.init())
            ])

        //when
        sut.prepareFormatedModel(model)
        guard let modelTransactions = model.transactions else {
            XCTFail("Model.transactions = nil")
            return
        }

        // Then
        XCTAssertEqual(spy.formattedModel[0].id, "\(modelTransactions[0].id!) - \(modelTransactions[0].description!)")
        XCTAssertEqual(spy.formattedModel[0].amount, modelTransactions[0].amount)
        XCTAssertTrue(spy.preparedFormatedModelCalled, "preparedFormatedModel(:[ListModel]) should ask the presenter to do something")
    }
}
