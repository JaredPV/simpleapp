//
//  ListPresenterTests.swift
//  SimpleApp
//
//  Created by Genar Codina Reverter on 04/08/18.
//  Copyright (c) 2018 Genar Codina Reverter. All rights reserved.
//

@testable import SimpleApp
import XCTest

class ListPresenterTests: XCTestCase {
  // MARK: Subject under test

  var sut: ListPresenter!

  // MARK: Test lifecycle

  override func setUp() {
    super.setUp()
    setupListPresenter()
  }

  override func tearDown() {
    super.tearDown()
  }

  // MARK: Test setup

  func setupListPresenter() {
    sut = ListPresenter()
  }

  // MARK: Test doubles

  class ListDisplayLogicSpy: ListPresenterToViewProtocol {
    var transactionsLabel: UILabel!

    var accountLabel: UILabel!

    var messageLabel: UILabel!

    var transactionTableView: UITableView!

    var formatedData: [ListModel]?

    var refreshCalled = false

    func refresh() {
        refreshCalled = true
    }
  }

  // MARK: Tests

  func testRefresh() {
    // Given
    let spy = ListDisplayLogicSpy()
    sut.view = spy
    let response = [
        ListModel(id: "ID", before: "100", amount: "10", after: "90", date: "2/12/2018"),
        ListModel(id: "ID", before: "100", amount: "10", after: "90", date: "2/12/2018"),
        ListModel(id: "ID", before: "100", amount: "10", after: "90", date: "2/12/2018"),
        ListModel(id: "ID", before: "100", amount: "10", after: "90", date: "2/12/2018"),
        ListModel(id: "ID", before: "100", amount: "10", after: "90", date: "2/12/2018")
    ]

    // When
    sut.preparedFormatedModel(response)

    // Then
    XCTAssertTrue(spy.refreshCalled, "preparedFormatedModel(response:) should ask the view controller to display the result")
    guard let data = spy.formatedData else {
        XCTFail("data is nil")
        return
    }
    XCTAssertEqual(data[0].id, response[0].id)
    XCTAssertEqual(data[0].before, response[0].before)
    XCTAssertEqual(data[0].amount, response[0].amount)
    XCTAssertEqual(data[0].after, response[0].after)
    XCTAssertEqual(data[0].date, response[0].date)
  }
}
